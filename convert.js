function numbersToWords () {
    var thou = [""];
    var onesP = ["", "one", "two", "three", "four", "five","six", "seven","eight","nine", "ten"];
    var teens = ['','eleven','twelve','thirteen','fourteen','fifteen','sixteen','seventeen','eighteen','nineteen'];
    var tensP = ['', 'ten','twenty','thirty','fourty','fifty','sixty','seventy','eighty','ninety'];

    for (i=1; i<=1001; i++) {
        if (i % 10 == i) {
            thou.push(onesP[i] + " ");
        }
        else if (i % 10 == 0) {
            thou.push(tensP[(i / 10)] + " ");
        }
        else if (i % 20 == i){
            thou.push(teens[i%20-10] + " ");
        }
        else if (i % 100 == i) {
            thou.push(tensP[Math.floor(i/10)] + " " + onesP[i%10]);
        }
        else if (i % 1000 == i) {
            thou.push(" " + onesP[Math.floor(i / 100)] + " hundred " + thou[i%100]);
        }
        else if (i % 1000==0) {
            break;}
    }
    thou.push(" One Thousand!")
    return thou;
};
document.write(numbersToWords());